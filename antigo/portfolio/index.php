﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php 
 header("Content-Type: text/html; charset=utf-8",true); 
 header("Cache-Control: no-cache, must-revalidate"); 
?> 
<meta name="y_key" content="9d4475fc3d6d12b3">
<meta name="google-site-verification" content="B-mtmdPWghMsakV3q4K0-slPgB1JdnMUZfjCb2VHylE" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cria Ideias</title>
<script type="text/javascript" src="../script/swfobject.js"></script>
<script type="text/javascript" src="../script/swfaddress.js"></script>
<script type="text/javascript">
		var params = {
			quality: "high",
			scale: "noscale",
			wmode: "window",
			allowscriptaccess: "always",
			bgcolor: "#BDB97A"
		};
		var flashvars = {
			theSWF: "portfolio.swf"
		};
		var attributes = {
			id: "flashcontent",
			name: "flashcontent"
		};
	
		function setFlashSize(flashWidth,flashHeight){
			var size = [0,0];
			if( typeof( window.innerWidth ) == 'number' ) {
				size = [window.innerWidth, window.innerHeight];
			} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
				size = [document.documentElement.clientWidth, document.documentElement.clientHeight];
			} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
				size = [document.body.clientWidth, document.body.clientHeight];
			}
			window.onresize = function() {
				document.getElementById("flashcontent").style.minWidth = flashWidth+"px";
				document.getElementById("flashcontent").style.minHeight = flashHeight+"px";
				document.getElementById("flashcontent").style.width = size[0] < flashWidth ? flashWidth+"px" : "100%";
				document.getElementById("flashcontent").style.height = size[1] < flashHeight ? flashHeight+"px" : "100%";
			};
			window.onload = function(){
				window.onresize();
			}
		}
		swfobject.embedSWF("preloader.swf", "flashMovie", "100%", "100%", "9.0.0","expressInstall.swf", flashvars, params, attributes);
		setFlashSize(900,550); //set the minimum width and height of your Flash movie
		//]]>
</script>
<style type="text/css">
	/*hide from ie on mac\*/
	html, body{
		margin:0;
		padding:0;
		height:100%;
		width: 100%;
		background-color:#BDB97A;
	}
	#flashcontent {
		width: 100%;
		height: 100%;
		overflow:hidden;
	}
	/* end hide */
</style>
</head>
<body>
	<div id="flashcontent">
        <div id="flashMovie">
            <h1>Para visualizar esse site voce precisa do Flash Player 9.0</h1>
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
        </div>
    </div>
</body>
</html>
