$(document).ready(function() {
	
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
        return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
  
	function clientSlider(selector, xml)
	{
	  
		function slidePhotos(){
			
			var preHtml = "<img src='";
			var titleHtml = "' title='";
			var altHtml = "' alt='";
			var posHtml = "' />";
			
			var images = new Array(3);
			
			for(var i=0; i < 3; i++)
			{
				if(count > content.length-1) count = 0;
				var newHtml = preHtml + content[count] + titleHtml + contentTitle[count] + altHtml + contentTitle[count] + posHtml;
				images[i] = newHtml;
				count++;
			}
			
			$(selector).html(images[0] + images[1] + images[2]).hide().slideFadeToggle(1500);
			
		}
		
		var items = 0;
		var content = new Array(10);
		var contentTitle = new Array(10);
		var count = 0;
		
		$.get(xml, function(data){
			$(data).find('cliente').each(function(){
				var $slide = $(this);
				content[items] = $slide.find('image').text();
				contentTitle[items] = $slide.find('name').text();
				items++;
			})
			slidePhotos();
			var slideInterval = setInterval(slidePhotos, 4000);
		})
		
	}
	
	clientSlider($('#clientImg'), "clientes/clientes.xml");
  
});
