<div id="sidebar" class="grid_4">
<?php	if ( is_active_sidebar( 'primary-widget-area' ) ) : ?>
	<ul class="sidebar-list"><?php dynamic_sidebar( 'primary-widget-area' ); ?></ul>
<?php endif; ?>
</div>