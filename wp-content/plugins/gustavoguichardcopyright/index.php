<?php 
/*
  Plugin Name: Gustavo Guichard Cria Copyright
  Description: Mostra informações de copyright no Dashboard
  Version: 0.1
  Author: Gustavo Guichard
  Author URI: http://gustavoguichard.com/
*/


function modify_footer_admin () {
	echo 'Criado por <a href="http://gustavoguichard.com">Gustavo Guichard</a> e <a href="http://criaideias.com.br">Cria Ideias</a>.';
	echo ' Suportado por <a href="http://WordPress.org">WordPress</a>.';
}

add_filter('admin_footer_text', 'modify_footer_admin');


?>