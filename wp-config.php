<?php
/** 
 * A configuração de base do WordPress
 *
 * Este ficheiro define os seguintes parâmetros: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, e ABSPATH. Pode obter mais informação
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} no Codex. As definições de MySQL são-lhe fornecidas pelo seu serviço de alojamento.
 *
 * Este ficheiro é usado para criar o script  wp-config.php, durante
 * a instalação, mas não tem que usar essa funcionalidade se não quiser. 
 * Salve este ficheiro como "wp-config.php" e preencha os valores.
 *
 * @package WordPress
 */

// ** Definições de MySQL - obtenha estes dados do seu serviço de alojamento** //
/** O nome da base de dados do WordPress */
define('DB_NAME', 'cria');

/** O nome do utilizador de MySQL */
define('DB_USER', 'root');

/** A password do utilizador de MySQL  */
define('DB_PASSWORD', 'root');

/** O nome do serviddor de  MySQL  */
define('DB_HOST', 'localhost:8889');

/** O "Database Charset" a usar na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O "Database Collate type". Se tem dúvidas não mude. */
define('DB_COLLATE', '');

/**#@+
 * Chaves Únicas de Autenticação.
 *
 * Mude para frases únicas e diferentes!
 * Pode gerar frases automáticamente em {@link https://api.wordpress.org/secret-key/1.1/salt/ Serviço de chaves secretas de WordPress.org}
 * Pode mudar estes valores em qualquer altura para invalidar todos os cookies existentes o que terá como resultado obrigar todos os utilizadores a voltarem a fazer login
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f4 j4(<T53km[y|waEn3N{au^l@2ANFob!5/)4%z0&7PRGm,jy5)UP18)czRX;}h');
define('SECURE_AUTH_KEY',  '#(hduHQl/u`1FJ/$FSe#qP~CS>FMYwB`bbVd]SfzZpUxL9Lxm;LzFY~y>?#<dS)8');
define('LOGGED_IN_KEY',    '#OklR C)}4`3_<mfQ<=MpDuj<c}4ruhHWI~$VhKkSMdl0(rdHNY}R)7mC/;@ABkh');
define('NONCE_KEY',        'aP93] uy$ahb(N>Q$RqZ]aXz7aS;PklMaBK>#nU(qNoyE&)ZBMMG3RaxvJ|,HQKS');
define('AUTH_SALT',        '7#A&F AI@8V7ZgY4as:ER?U? jOYjn@ZRBxt^>*+E<{dG;(JQl%m/qs7o@9~(*F+');
define('SECURE_AUTH_SALT', 'D+vkB_hAR=ZcOcNH,qNqMSa~GZYy&[[X$`vNz=2R];]Qch.QcbQOMheKk.B3 ^&7');
define('LOGGED_IN_SALT',   '), fOnt#nQ;ucHN/h1D(K}|XbhFs]^N`suG%uQRU,`0A_j Z1IAilk7YL^oXeCx)');
define('NONCE_SALT',       '65p+zm.bm]GCyFy9fgbt7bczkeN/trN~:B C8``EUE9]3H61Hot$HbqEK@w?x)  ');

/**#@-*/

/**
 * Prefixo das tabelas de WordPress.
 *
 * Pode suportar múltiplas instalações numa só base de dados, ao dar a cada
 * instalação um prefixo único. Só algarismos, letras e underscores, por favor!
 */
$table_prefix  = 'wp_';

/**
 * Idioma de Localização do WordPress, Inglês por omissão.
 *
 * Mude isto para localizar o WordPress. Um ficheiro MO correspondendo ao idioma
 * escolhido deverá existir na directoria wp-content/languages. Instale por exemplo
 * pt_PT.mo em wp-content/languages e defina WPLANG como 'pt_PT' para activar o
 * suporte para a língua portuguesa.
 */
define ('WPLANG', 'pt_PT');

/**
 * Para developers: WordPress em modo debugging.
 *
 * Mude isto para true para mostrar avisos enquanto estiver a testar.
 * É vivamente recomendado aos autores de temas e plugins usarem WP_DEBUG
 * no seu ambiente de desenvolvimento.
 */
define('WP_DEBUG', false);

/* E é tudo. Pare de editar! Bom blogging!. */

/** Caminho absoluto para a pasta do WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Define as variáveis do WordPress e ficheiros a incluir. */
require_once(ABSPATH . 'wp-settings.php');
